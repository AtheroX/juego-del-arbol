﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryCollider : MonoBehaviour {

    public Text texto;
    public GameObject pruebaTexto;
    RaycastHit hit;
    public Camera Camara;
    InventoryManager manager;
    Vector3 forward;
    public float ray;

    void Start() {
        manager = GameObject.Find("Player").GetComponent<InventoryManager>();
        //    manager = GetComponent<InventoryManager>();
    }



    private void OnTriggerEnter(Collider other) {
        pruebaTexto.SetActive(true);
        texto.text = "Pulse E para recoger " + other.gameObject.name;
        //   if (Input.GetKeyDown(KeyCode.E))
        // {
        print("He encontrado tu objeto");
        CollectibleObject col = other.GetComponent<CollectibleObject>();
        manager.AgregarAlgoAlInventario(col.id, col.cantidad);
        Destroy(other.gameObject);
        pruebaTexto.SetActive(false);
        //}
    }
}