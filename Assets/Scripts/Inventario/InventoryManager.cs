﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour {

    [System.Serializable]
    public struct ObjetoInventarioId {
        public int id;
        public int cantidad;

        public ObjetoInventarioId(int id, int cantidad) {
            this.id = id;
            this.cantidad = cantidad;
        }
    }

    public InventoryDataBase baseDatos;
    public List<ObjetoInventarioId> inventario;
    public ObjetoInventarioId[] invMano;

    public void AgregarAlgoAlInventario(int id, int cantidad) {
        for (int i = 0; i < inventario.Count; i++) {
            if (inventario[i].id == id) {
                inventario[i] = new ObjetoInventarioId(inventario[i].id, inventario[i].cantidad + cantidad);
                //ActualizarInventario();
                return;
            }
        }
        inventario.Add(new ObjetoInventarioId(id, cantidad));
        //ActualizarInventario();
    }

    GameObject basura;

    public void EliminarAlgoDeInventario(int id, int cantidad) {
        for (int i = 0; i < inventario.Count; i++) {
            if (inventario[i].id == id) {
                inventario[i] = new ObjetoInventarioId(inventario[i].id, inventario[i].cantidad - cantidad);
                if (inventario[i].cantidad <= 0) {
                    inventario.Remove(inventario[i]);
                }
                //ActualizarInventario();
                return;
            }
        }
        Debug.LogError("No existe el objeto a eliminar");
    }

    public bool clicControl = false;

    public void IntercambiarPuestos(int i1, int i2) {
        clicControl = false;
        ObjetoInventarioId i = inventario[i1];
        inventario[i1] = inventario[i2];
        inventario[i2] = i;
        //ActualizarInventario();
    }

    public void Start() {
       // ActualizarInventario();
    }


    /*public InventoryObjectInterface prefab;
    List<InventoryObjectInterface> pool = new List<InventoryObjectInterface>();

    public void ActualizarInventario() {
        print("InventarioActualizado");
        for (int i = 0; i < pool.Count; i++) {
            if (i < inventario.Count) {
                ObjetoInventarioId o = inventario[i];
                pool[i].sprite.sprite = baseDatos.DBInventario[o.id].sprite;
                pool[i].cantidad.text = o.cantidad.ToString();
                pool[i].id = i;

                pool[i].boton.onClick.RemoveAllListeners();
                //pool[i].boton.onClick.AddListener(() => gameObject.SendMessage(baseDatos.DBInventario[o.id].funcion, o.id, SendMessageOptions.DontRequireReceiver));
                pool[i].boton.onClick.AddListener(() => gameObject.SendMessage(baseDatos.DBInventario[o.id].funcion, o.id, SendMessageOptions.DontRequireReceiver));

                pool[i].gameObject.SetActive(true);
            } else {
                pool[i].gameObject.SetActive(false);
            }
        }
        if (inventario.Count > pool.Count) {
            for (int i = pool.Count; i < inventario.Count; i++) {
                InventoryObjectInterface oi = Instantiate(prefab, inventarioUI);
                pool.Add(oi);

                oi.transform.position = Vector3.zero; // 
                oi.transform.localScale = Vector3.one;

                ObjetoInventarioId o = inventario[i];
                pool[i].sprite.sprite = baseDatos.DBInventario[o.id].sprite;
                pool[i].cantidad.text = o.cantidad.ToString();
                pool[i].id = i;
                pool[i].manager = this;

                pool[i].boton.onClick.RemoveAllListeners();
                pool[i].boton.onClick.AddListener(() => gameObject.SendMessage(baseDatos.DBInventario[o.id].funcion, o.id, SendMessageOptions.DontRequireReceiver));

                pool[i].gameObject.SetActive(true);
            }
        }
    }*/

    public void TirarAlgoDelInventario(int id) {
        inventario.Remove(inventario[id]);
        //ActualizarInventario();
    }

    public GameObject paquete;
    GameObject paqueteClon;
    public float lanzarPaquete;

    public void InstanciaObjetoTirado(int id) {
        paquete.GetComponent<CollectibleObject>().id = inventario[id].id;
        paquete.GetComponent<CollectibleObject>().cantidad = inventario[id].cantidad;
        paqueteClon = Instantiate(paquete, transform.position + Vector3.Normalize(transform.forward) + new Vector3(0f, 1.2f, 0f), Quaternion.identity);
        paqueteClon.GetComponent<Rigidbody>().AddForce(Vector3.Normalize(transform.forward) * lanzarPaquete);
    }

    public void Pocion() {
        EliminarAlgoDeInventario(0, 1);
    }

    int objetoEncontrado;

    /*public int BuscarEnInventario(int idObjectDB) {
        for (int i = 0; i < pool.Count; i++) {
            if (inventario[i].id == idObjectDB) {
                return i;
            }
        }

        return -1;

    }*/

    [SerializeField]
    public Transform inventarioUI;
    public Transform RopaUI;
    public Transform AyHUI;
    public Transform UtilesUI;

    
    /* public void slotsRopa(int id, int slotDefinido)
     {
         objetoEncontrado = BuscarEnInventario(id);
         print(objetoEncontrado);
         invMano[slotDefinido].id = inventario[objetoEncontrado].id;
         invMano[slotDefinido].cantidad = inventario[objetoEncontrado].cantidad;
         //RopaUI.GetChild(slotDefinido).GetComponent<Image>().sprite = inventarioUI.GetChild(objetoEncontrado).GetComponent<Image>().sprite;
         seleccionHub(id, slotDefinido);
         EliminarAlgoDeInventario(id, 1);
     }

     public void seleccionHub(int id, int slotDefinido)
     {
         switch (baseDatos.DBInventario[id].Hub)
         {
             case "Ropa":
                 RopaUI.GetChild(slotDefinido).GetComponent<Image>().sprite = inventarioUI.GetChild(objetoEncontrado).GetComponent<Image>().sprite;
                 break;
             case "Arma&Herramienta":
                 AyHUI.GetChild(slotDefinido).GetComponent<Image>().sprite = inventarioUI.GetChild(objetoEncontrado).GetComponent<Image>().sprite;
                 break;
             case "Utiles":
                 UtilesUI.GetChild(slotDefinido).GetComponent<Image>().sprite = inventarioUI.GetChild(objetoEncontrado).GetComponent<Image>().sprite;
                 break;
         }
     }

     public void Casco(int id)
     {

         //Al tratarse de cascos estos irán al slot 0

         switch (id)
         {
             case 1:
                 print("Has cogido este casco, funciona correctamente.");
                 slotsRopa(id, 0);
                 break;
             case 2:
                 print("No coge bien el valor.");
                 break;
         }
     }

     public void Armadura(int id)
     {

         //Las armaduras se guardarán en el slot 1

         switch (id)
         {
             case 1:
                 print("Has cogido este casco, funciona correctamente.");
                 slotsRopa(id, 0);
                 break;
             case 2:
                 print("No coge bien el valor.");
                 break;
         }
     }

     public void Zapatos(int id)
     {

         //Mochilas al slot 2

         switch (id)
         {
             case 1:
                 print("Has cogido este casco, funciona correctamente.");
                 slotsRopa(id, 0);
                 break;
             case 2:
                 print("No coge bien el valor.");
                 break;
         }
     }

     public void Mochilas(int id)
     {

         //Mochilas al slot 3

         switch (id)
         {
             case 1:
                 print("Has cogido este casco, funciona correctamente.");
                 slotsRopa(id, 0);
                 break;
             case 2:
                 print("No coge bien el valor.");
                 break;
         }
     }*/




}