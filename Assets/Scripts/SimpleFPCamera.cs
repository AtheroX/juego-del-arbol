﻿using UnityEngine;
using System.Collections;

public class SimpleFPCamera : MonoBehaviour  {

    //player
    public float Sensibilidad = 2.0f;
    public float Largo;
    public Rigidbody Rg;
    public CapsuleCollider Prueba;
    public float Velocidad;
    public Transform Camara;
    public Arbol Arbol_script;
    public Roca Roca_script;
    public float Peso;
    //Móvil
    public bool movil_estado;
    public GameObject movil;
    //Vehiculo
    private VehiculoControl VControl;
    private bool EnVehiculo;
    private Vector3 Salida;
    private Transform Vehiculo;
    private PropiedadesAuto.EstadoPuertas PropVehiculos;
    private GUIController GUIController;

    void Awake(){

        Cursor.visible = false;
        GUIController = GameObject.Find("00scripts").GetComponent<GUIController>();
    }

    void FixedUpdate(){

        Mirar();
        if (EnVehiculo == false)
        {
            Movimiento();
            Interactuar();        
        }
        else Conduciendo();

        
    }
    private void Update() {
        if (Input.GetKeyDown(KeyCode.V)) {

            movil_estado = !movil_estado;
            
            if (movil_estado) {
                movil.SetActive(true);
                Cursor.visible = true;
                movil.GetComponent<Animator>().SetBool("Estado_fuera", true);
                movil.GetComponent<Animator>().SetBool("Estado_dentro", false);

            } else {

                Cursor.visible = false;
                movil.GetComponent<Animator>().SetBool("Estado_fuera", false);
                movil.GetComponent<Animator>().SetBool("Estado_dentro", true);
                //cerrar ventanas al esconder el telefono
                GUIController.cerrar_ventanas();
                movil.SetActive(false);

            }


        }
    }
    void Movimiento(){
        if (!movil_estado) {
            Rg.velocity = ((transform.forward * Input.GetAxis("Vertical") + transform.right * Input.GetAxis("Horizontal")) * Velocidad) + Vector3.up * Rg.velocity.y;
            Rg.MovePosition(transform.position + ((transform.forward * Input.GetAxis("Vertical") + transform.right * Input.GetAxis("Horizontal")) * Velocidad) + Vector3.up * Rg.velocity.y);
        }
    }

    void Mirar() {
        if (!movil_estado) {
            //Rotar jugador izquierda a derecha
            transform.eulerAngles += new Vector3(0, Input.GetAxis("Mouse X") * Sensibilidad, 0);

            //Si la inclinacion de la camara esta entre 85 positivo y negativo se permite rotarla
            if (Camara.transform.eulerAngles.x < 85 || Camara.transform.eulerAngles.x > -85) {

                Camara.transform.eulerAngles += new Vector3(-Input.GetAxis("Mouse Y") * Sensibilidad, 0, 0);
            }
        }
    }

    void Interactuar(){

        RaycastHit Hit;
        Physics.Raycast(Camara.transform.position, Camara.transform.forward, out Hit, Largo);
        Debug.DrawRay(Camara.transform.position, Camara.transform.forward);
        Debug.Log(GameObject.Find(Hit.collider.gameObject.name));

        if (Input.GetMouseButtonDown(0)){
            if (Hit.collider == null) return;
            switch (Hit.collider.tag){

              case "Objeto":
              break;
                case "Rock":
                    Roca_script = Hit.collider.GetComponent<Roca>();

                    //Al clicarle bajarle la vida 
                    if (Input.GetButtonDown("Fire1")){

                        Roca_script.Rock_HP--;
                        Roca_script.player = this.gameObject;

                    }

                    break;

                case "Tree":
                    Arbol_script = Hit.collider.GetComponent<Arbol>();

                    //Al clicarle bajarle la vida 
                    if (Input.GetButtonDown("Fire1")){

                        Arbol_script.Tree_HP--;
                        Arbol_script.player = this.gameObject;
                    }

                    break;

            }
            Debug.DrawRay(Camara.transform.position, Camara.transform.forward * Largo, Color.red, 0.1f);
        }

        if (Input.GetButtonDown("Interactuar")){

            switch (Hit.collider.tag){

                case "Plantacion":

                    //Si al apuntar es una plantacion, se coge el script de plantacion y se asigna dentro de el al player
                    
                    Plantacion plantacion = Hit.collider.GetComponent<Plantacion>();
                    plantacion.player_script = this.gameObject.GetComponent<SimpleFPCamera>();

                    plantacion.pos = Hit.point;
                    plantacion.Terreno = plantacion.gameObject;

                    plantacion.Plantar();

                    Debug.Log("Plantado");


                break;

                case "Planta":

                    plantacion_planta plantacion_planta = Hit.collider.GetComponent<plantacion_planta>();
                    plantacion_planta.player_script = this.gameObject.GetComponent<SimpleFPCamera>();

                    if (plantacion_planta.listo) {
                        plantacion_planta.listo = false;
                        plantacion_planta.recogido = true;
                        Debug.Log("Recogido");

                    }
                    break;

                case "PuertaVehiculo":
                    PropVehiculos = Hit.collider.gameObject.GetComponent<PropiedadesAuto>().NombrePuerta[Hit.collider.name];
                    if (PropVehiculos.Ocupado == false){

                        Vehiculo = Hit.collider.transform;
                        Salida = Vehiculo.transform.position - transform.position;
                        VControl = Vehiculo.GetComponent<VehiculoControl>();
                        VControl.Encendido = true;
                        EnVehiculo = true;
                    }
                break;
            }
        }
    }

    void Conduciendo(){

        Prueba.enabled = false;
        transform.eulerAngles = new Vector3(Vehiculo.eulerAngles.x, transform.eulerAngles.y, Vehiculo.eulerAngles.z);
        Rg.isKinematic = true;
        transform.position = Vehiculo.position + PropVehiculos.Ubicacion;

        if (Input.GetButtonDown("Jump")){

            transform.position -= new Vector3(Salida.x, transform.position.y, Salida.y);
            Rg.isKinematic = false;
            EnVehiculo = false;
            Prueba.enabled = true;
            VControl.Encendido = false;

        }
    }

    
}