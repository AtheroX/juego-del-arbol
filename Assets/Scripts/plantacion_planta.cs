﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plantacion_planta : MonoBehaviour {

    public float Time_max = 300;
    public float Timer;
    public float media_time;

    public SimpleFPCamera player_script;
    public Plantacion mi_plantacion;

    public bool plantado = true;
    public bool listo = false;
    public bool recogido = false;
    public Vector3 pos_i;
    public Vector3 pos_f;


    // Use this for initialization
    void Start () {
        //le pongo la escala a 10,10,20 porque el suelo lo deforma. tambien añado su posicion inicial y calculo la final.
        transform.localScale = new Vector3(10, 10, 20);
        pos_i = transform.position;
        pos_f = pos_i + Vector3.up * 0.2f;
	}
	
	// Update is called once per frame
	void Update () {
        //Si se planta se activa un timer y se calcula la media de crecimiento por segundo. Despues hago un lerp sumandole ese crecimiento.
        if (plantado) {
            Timer += Time.deltaTime;
            media_time = Timer / Time_max;
            transform.position = Vector3.Lerp(pos_i,pos_f,media_time);

        }
        //Si el timer llega al tiempo establecido, restablece el timer y pasa de estado plantado a listo para recoger.
        if (Timer >= Time_max) {
            plantado = false;
            Timer = 0;
            listo = true;
        }
        //Si alguien lo recoge, se destruye y se manda que ha sido recogido (para que más adelante se añada al inventario.
        if (recogido) {
            Destroy(this.gameObject);
            recogido = true;
        }
    }
}
