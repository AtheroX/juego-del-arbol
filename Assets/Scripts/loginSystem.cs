﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class loginSystem : MonoBehaviour {

    public Text inputUserName;
    public InputField inputPassword;
    serverResponse respuesta;
    public string sceneName;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}
    public void loginButton() {
        
        StartCoroutine(login(inputUserName.text, inputPassword.text));
    }
    public IEnumerator login(string username,string password) {
        WWWForm form = new WWWForm();
        form.AddField("passwordLogin", password);
        form.AddField("userLogin", username);

        WWW request = new WWW("https://cristiantechfolio.000webhostapp.com/login.php", form);
        yield return request;
        respuesta = JsonUtility.FromJson<serverResponse>(request.text);
        if (respuesta.checkError())
        {
            SceneManager.LoadScene(sceneName);                
        }
    }
}
[System.Serializable]
public class serverResponse {
    public bool checkError() {
        if (this.error != "0")
        {
            Debug.Log("Error: " + this.error + " info: " + this.message);
            return false;
        }
        else {
            Debug.Log("Login as " + this.info.user + " success.");
            return true;
        }
    }
    public string error;
    public string message;
    public inf info;
}

[System.Serializable]
public class inf {
    public string user;
    public string email;
}