﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using serverRQ;

public class GUIController : MonoBehaviour {

    //Settings y shop
    //Todos los BG posibles del movil
    public Sprite[] Movil_BG;
    //Todas los iconos de las apps posibles del movil
    public Sprite[] Movil_Apps;
    //Boton de BG's en si de Settings
    public Button[] Movil_BG_Buttons;

    //Todas los GO de las aplicaciones
    public GameObject[] Movil_Apps_GO;

    //Bools para saber si han sido comprados los BG's y script de los botones para cambiar el BG
    public bool[] Movil_BG_compradas;

    //para pasarselo a Telefono_BG
    public GameObject BG_Object, Backgrounds, BT_Settings_prefab, BT_ejemplo;


    //
    //Banckia
    public Text txt_dinero_mano_HUD, txt_dinero_movil_banco;
    public banco banco_connection;
    public bool algo;

    void Awake() {
        banco_connection = new banco();    
    }

    void Start () {
        StartCoroutine(banco_connection.View("1"));
        algo = true;
	}

    void Update() {
        if ((banco_connection.running == false)&&(algo)) {
            txt_dinero_movil_banco.text = banco_connection.respuesta.info.dineroBanco.ToString();
            txt_dinero_mano_HUD.text = banco_connection.respuesta.info.dineroMano.ToString();
            Debug.Log("patata");
            algo = false;
        }
    }

    public void Store_BG_0(int n) {
        StartCoroutine(banco_connection.Add("1", accionBanco.sacar, 100));
        if (!Movil_BG_compradas[n]) {
            //marcar como comprado
            Movil_BG_compradas[n] = true;

            //Crear objeto y hacerlo child de una carpeta "BackGrounds"
            GameObject boton = Instantiate(BT_Settings_prefab);
            boton.transform.parent = Backgrounds.transform;

            //Asignarle la imagen recien clicada
            boton.GetComponent<Telefono_BG_Botones>().BG_Button_ImageCompontent = EventSystem.current.currentSelectedGameObject.GetComponent<Image>();

            //Asignarle el objeto fondo de pantalla del telefono
            boton.GetComponent<Telefono_BG_Botones>().BG_Object = BG_Object;

            //Asignarle el tamaño, posicion y rotacion debida 
            boton.transform.localScale = BT_ejemplo.transform.localScale;
            boton.transform.rotation = BT_ejemplo.transform.rotation;
            boton.transform.position = BT_ejemplo.transform.position;
        } else {
            Debug.Log("puto");
        }
    }

    public void cerrar_ventanas() {
        for(int i = 0; i < Movil_Apps_GO.Length; i++) {
            Movil_Apps_GO[i].SetActive(false);
        }
    }
}
