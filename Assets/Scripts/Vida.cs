﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Vida : MonoBehaviour {

    public int VidaActual = 100;
    private int VidaInicial = 100;
    private int maxhealth = 100;
    public Slider VidaSlider;

    // Use this for initialization
    void Start () {
        VidaSlider.value = VidaActual;
        VidaActual = VidaInicial;
    }
	
	// Update is called once per frame
	void Update () {
        VidaSlider.value = VidaActual;
        if(VidaActual > maxhealth)
        {
            VidaActual = 100;
        }
    }
}
