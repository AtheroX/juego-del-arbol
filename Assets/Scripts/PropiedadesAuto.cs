﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropiedadesAuto : MonoBehaviour{
    public Dictionary<string, EstadoPuertas> NombrePuerta = new Dictionary<string, EstadoPuertas>();
    [SerializeField] public  EstadoPuertas[] Puertas;
    [System.Serializable]
    public class EstadoPuertas
    {
        public string nombre;
        public bool Ocupado;
        public Vector3 Ubicacion;
    }
    private void Awake()
    {
        for (int I = 0; I < Puertas.Length; I++)
        {
            NombrePuerta.Add(Puertas[I].nombre, Puertas[I]);
        }
    }
}
