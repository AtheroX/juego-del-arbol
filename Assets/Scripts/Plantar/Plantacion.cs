﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plantacion : MonoBehaviour {

    public SimpleFPCamera player_script;

    public GameObject[] semilla_plantacion;
    public List<GameObject> cosas_plantadas = new List<GameObject>();
    public GameObject Terreno;
    public Vector3 pos;
    

    public GameObject planta;

    void Start () {
	}
	
	void Update () {
                


    }
    public void Plantar() {
        planta = Instantiate(semilla_plantacion[0], new Vector3(pos.x,pos.y-0.325f,pos.z), Quaternion.Euler(0, 0, 0));

        planta.GetComponent<plantacion_planta>();

        planta.transform.parent = Terreno.transform;
        planta.transform.localScale = new Vector3(5, 5, 50);
        planta.transform.rotation = new Quaternion(0, 0, 0, 0);

        planta.GetComponent<plantacion_planta>().mi_plantacion = this;
    }
}
